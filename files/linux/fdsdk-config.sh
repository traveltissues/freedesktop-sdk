#! /bin/bash

set -eu
# set -x

arch=$1

# Modify the kernel config for additional features

case "%{arch}" in
    aarch64)
        module VIDEO_ROCKCHIP_ISP1
        module VIDEO_ROCKCHIP_RGA
        module VIDEO_ROCKCHIP_VDEC
        module VIDEO_HANTRO
        module VIDEO_OV8858
        module VIDEO_IMX258
        module VIDEO_DW9714
    ;;
esac

case "%{arch}" in
    aarch64)
        module ROCKCHIP_SARADC
        enable ROCKCHIP_MBOX
        module ROCKCHIP_THERMAL
        enable ROCKCHIP_RGB
    ;;
esac

case "%{arch}" in
    aarch64)
        module CRYPTO_DEV_ROCKCHIP
    ;;
esac

case "%{arch}" in
    aarch64)
        module PHY_ROCKCHIP_DPHY_RX0
    ;;
esac

case "%{arch}" in
    aarch64)
        enable BACKLIGHT_CLASS_DEVICE
    ;;
esac

case "%{arch}" in
    aarch64)
        module V4L2_FLASH_LED_CLASS
    ;;
esac

# Kernel Config Options
enable DEVTMPFS
enable CGROUPS
enable INOTIFY_USER
enable SIGNALFD
enable TIMERFD
enable EPOLL
enable NET
enable SYSFS
enable PROC_FS
enable FHANDLE

# Enable access to kernel config through /proc/config.gz
enable IKCONFIG
enable IKCONFIG_PROC

# Kernel crypto/hash API
module CRYPTO_AES
module CRYPTO_ARC4
module CRYPTO_CBC
module CRYPTO_CMAC
module CRYPTO_CRC32
module CRYPTO_CRC32C
module CRYPTO_CRCT10DIF
module CRYPTO_DES
module CRYPTO_ECB
module CRYPTO_GHASH
module CRYPTO_HMAC
module CRYPTO_MD4
module CRYPTO_MD5
module CRYPTO_SHA1
module CRYPTO_SHA256
module CRYPTO_SHA512
module CRYPTO_XTS

enable CRYPTO_USER_API_HASH
enable CRYPTO_USER_API_SKCIPHER

case "$arch" in
    i686)
        module CRYPTO_CRC32C_INTEL
        module CRYPTO_CRC32_PCLMUL
        module CRYPTO_AES_NI_INTEL
    ;;
    x86_64)
        module CRYPTO_CRC32C_INTEL
        module CRYPTO_CRC32_PCLMUL
        module CRYPTO_AES_NI_INTEL
        module CRYPTO_CRCT10DIF_PCLMUL
        module CRYPTO_GHASH_CLMUL_NI_INTEL
        module CRYPTO_SHA1_SSSE3
        module CRYPTO_SHA256_SSSE3
        module CRYPTO_SHA512_SSSE3
        module CRYPTO_DES3_EDE_X86_64
    ;;
    arm)
        module CRYPTO_SHA1_ARM
        module CRYPTO_SHA1_ARM_NEON
        module CRYPTO_SHA1_ARM_CE
        module CRYPTO_SHA2_ARM_CE
        module CRYPTO_SHA256_ARM
        module CRYPTO_SHA512_ARM
        module CRYPTO_AES_ARM
        module CRYPTO_AES_ARM_BS
        module CRYPTO_AES_ARM_CE
        module CRYPTO_GHASH_ARM_CE
        module CRYPTO_CRCT10DIF_ARM_CE
        module CRYPTO_CRC32_ARM_CE
    ;;
    aarch64)
        module CRYPTO_SHA256_ARM64
        module CRYPTO_SHA512_ARM64
        module CRYPTO_SHA1_ARM64_CE
        module CRYPTO_SHA2_ARM64_CE
        module CRYPTO_SHA512_ARM64_CE
        module CRYPTO_GHASH_ARM64_CE
        module CRYPTO_CRCT10DIF_ARM64_CE
        module CRYPTO_AES_ARM64
        module CRYPTO_AES_ARM64_CE
        module CRYPTO_AES_ARM64_CE_CCM
        module CRYPTO_AES_ARM64_CE_BLK
        module CRYPTO_AES_ARM64_NEON_BLK
        module CRYPTO_AES_ARM64_BS
    ;;
esac

case "$arch" in
esac

# udev will fail to work with legacy sysfs
remove SYSFS_DEPRECATED

# Boot is very slow with systemd when legacy PTYs are present
remove LEGACY_PTYS
remove LEGACY_PTY_COUNT

# Legacy hotplug confuses udev
value_str UEVENT_HELPER_PATH ""

# Userspace firmware loading not supported
remove FW_LOADER_USER_HELPER

# Some udev/virtualization requires
enable DMIID

# systemd-creds depends on this
enable DMI_SYSFS
enable FW_CFG_SYSFS

# Support for some SCSI devices serial number retrieval
enable BLK_DEV_BSG

# Required for PrivateNetwork= in service units
enable NET_NS
enable USER_NS

# Required for 9p support
enable NET_9P
enable NET_9P_VIRTIO
enable 9P_FS
enable 9P_FS_POSIX_ACL
enable 9P_FS_SECURITY
enable VIRTIO_PCI

# Strongly Recommended
enable IPV6
enable AUTOFS_FS
enable TMPFS_XATTR
enable TMPFS_POSIX_ACL
enable EXT4_FS_POSIX_ACL
enable EXT4_FS_SECURITY
enable SECCOMP
enable SECCOMP_FILTER
enable CHECKPOINT_RESTORE

# Required for CPUShares= in resource control unit settings
enable CGROUP_SCHED
enable FAIR_GROUP_SCHED

# Required for CPUQuota= in resource control unit settings
enable CFS_BANDWIDTH

# Required for IPAddressDeny=, IPAddressAllow= in resource control unit settings
enable CGROUP_BPF

# For UEFI systems
enable EFIVAR_FS
enable EFI_PARTITION
if has EFI_GENERIC_STUB; then
    enable EFI_ZBOOT
fi

# RT group scheduling (effectively) makes RT scheduling unavailable for userspace
remove RT_GROUP_SCHED

# Sound with QEMU
module SOUND
module SND
module SND_HDA_GENERIC

# Required for live boot
module SQUASHFS
enable SQUASHFS_ZLIB
module OVERLAY_FS

# Required by snapd
enable SQUASHFS_COMPILE_DECOMP_MULTI_PERCPU
enable SQUASHFS_DECOMP_MULTI_PERCPU
enable SQUASHFS_FILE_DIRECT
enable SQUASHFS_LZ4
enable SQUASHFS_LZO
enable SQUASHFS_XATTR
enable SQUASHFS_XZ
enable SQUASHFS_ZSTD

# erofs is potential replacement of squashfs on GNOME OS
module EROFS_FS
enable EROFS_FS_XATTR
enable EROFS_FS_POSIX_ACL
enable EROFS_FS_SECURITY
enable EROFS_FS_ZIP
enable EROFS_FS_ZIP_LZMA
enable EROFS_FS_ZIP_DEFLATE

# Some useful drivers when running as virtual machine
module VIRTIO_BALLOON
module VIRTIO_INPUT
module LIBNVDIMM
enable VIRTIO_PMEM
enable VIRTIO_NET
enable VIRTIO_BLK
enable SCSI_VIRTIO
enable VIRTIO_IOMMU

enable BLK_MQ_VIRTIO
module VIRTIO_CONSOLE
module VIRTIO_MMIO
module CRYPTO_DEV_VIRTIO

# Input
enable INPUT_EVDEV
enable INPUT_TOUCHSCREEN
enable INPUT_TABLET
module INPUT_MOUSEDEV
case "$arch" in
    i686|x86_64)
        module KEYBOARD_APPLESPI
    ;;
    aarch64)
        module INPUT_GPIO_VIBRA
        module KEYBOARD_PINEPHONE
    ;;
esac
module KEYBOARD_GPIO

# Touchscreens
module TOUCHSCREEN_ATMEL_MXT
module TOUCHSCREEN_DYNAPRO
module TOUCHSCREEN_EETI
module TOUCHSCREEN_ELO
module TOUCHSCREEN_FUJITSU
module TOUCHSCREEN_GOODIX
module TOUCHSCREEN_GUNZE
module TOUCHSCREEN_INEXIO
module TOUCHSCREEN_MCS5000
module TOUCHSCREEN_MTOUCH
module TOUCHSCREEN_PENMOUNT
module TOUCHSCREEN_SURFACE3_SPI
module TOUCHSCREEN_TOUCHIT213
module TOUCHSCREEN_TOUCHRIGHT
module TOUCHSCREEN_TOUCHWIN
module TOUCHSCREEN_TSC2007
module TOUCHSCREEN_TSC_SERIO
module TOUCHSCREEN_USB_COMPOSITE
enable TOUCHSCREEN_USB_E2I
module TOUCHSCREEN_WACOM_W8001
module TOUCHSCREEN_SILEAD

# tablets
module TABLET_SERIAL_WACOM4
module TABLET_USB_ACECAD
module TABLET_USB_AIPTEK
module TABLET_USB_HANWANG
module TABLET_USB_KBTAB
module TABLET_USB_PEGASUS

# needed by spice-vdagent
module VSOCKETS
module VIRTIO_VSOCKETS
module INPUT_UINPUT

# for virtualbox
case "$arch" in
    i686|x86_64)
        enable VIRT_DRIVERS
        module VBOXGUEST
    ;;
esac

# Hyper-V
case "$arch" in
    i686|x86_64|aarch64)
        module HYPERV
        module PCI_HYPERV
        module HYPERV_BALLOON
        module HYPERV_NET
        if has CONNECTOR; then
            module HYPERV_UTILS
        fi
    ;;
esac

# Xen
case "$arch" in
    i686|x86_64|aarch64)
        enable XEN
        module XEN_SCSI_FRONTEND
    ;;
esac

# VMWare
case "$arch" in
    i686|x86_64)
        module VMWARE_BALLOON
        module VMWARE_VMCI
        module VMWARE_PVSCSI
    ;;
esac

# Needed by some devices
case "$arch" in
    i686|x86_64)
        enable INTEL_TPMI
        enable INTEL_VSEC
        enable X86_INTEL_LPSS
    ;;
esac

# Device Tree and Open Firmware
case "$arch" in
    aarch64|arm)
        enable DTPM
        enable DTPM_CPU
        enable DTPM_DEVFREQ
    ;;
esac

# Wireguard
module WIREGUARD

# Dummy network driver
module DUMMY

# For wireless networks
enable WIRELESS
module CFG80211
enable CFG80211_WEXT
enable MAC80211
enable NETDEVICES
enable WLAN

# Wifi hardware
module IWLWIFI
module IWLMVM
module IWLDVM
module ATH9K
module ATH10K
module ATH10K_PCI
module RTW88
enable RTW88_8822BE
enable RTW88_8822CE
enable WLAN_VENDOR_ATH
module AR5523
module ATH10K_USB
module ATH11K
module ATH11K_PCI
module ATH5K
module ATH6KL
module ATH6KL_SDIO
module ATH6KL_USB
module ATH9K_HTC
module CARL9170
enable CARL9170_LEDS
module WIL6210
enable WIL6210_DEBUGFS
enable WIL6210_ISR_COR
module B43
enable B43_BUSES_BCMA_AND_SSB
enable B43_PHY_G
enable B43_PHY_HT
enable B43_PHY_LP
enable B43_PHY_N
enable B43_SDIO
module B43LEGACY
enable B43LEGACY_DMA_AND_PIO_MODE
module BRCMFMAC
module BRCMSMAC
enable BRCMFMAC_PCIE
enable BRCMFMAC_SDIO
enable BRCMFMAC_USB
module IWL3945
module IWL4965
module HERMES
enable HERMES_PRISM
enable HERMES_CACHE_FW_ON_INIT
module NORTEL_HERMES
module ORINOCO_USB
module PCI_HERMES
if has PCMCIA; then
    module PCMCIA_HERMES
fi
module PLX_HERMES
module P54_COMMON
module P54_PCI
module P54_USB
module MAC80211_HWSIM
module LIBERTAS
if has PCMCIA; then
    module LIBERTAS_CS
fi
enable LIBERTAS_MESH
module LIBERTAS_SDIO
module LIBERTAS_USB
module MWIFIEX
module MWIFIEX_PCIE
module MWIFIEX_SDIO
module MWIFIEX_USB
module MWL8K
module MT7601U
module MT7615E
module MT7663U
module MT76x0E
module MT76x0U
module MT76x2E
module MT76x2U
module MT7915E
module MT7921E
module MT7921U
module RT2400PCI
module RT2500PCI
module RT2500USB
module RT2800PCI
enable RT2800PCI_RT3290
enable RT2800PCI_RT33XX
enable RT2800PCI_RT35XX
enable RT2800PCI_RT53XX
module RT2800USB
enable RT2800USB_RT33XX
enable RT2800USB_RT3573
enable RT2800USB_RT35XX
enable RT2800USB_RT53XX
enable RT2800USB_RT55XX
module RT2X00
module RT61PCI
module RT73USB
module RTL8XXXU
module RTW88_8723DE
module RTW88_8821CE
module RTW89
module RTW89_8852AE
module RSI_91X
enable RSI_COEX
enable RSI_DEBUGFS
module RSI_USB
module ZD1211RW

# Ethernet hardware
module IGB
if has PCMCIA; then
    module PCMCIA_3C574
    module PCMCIA_3C589
fi
module TYPHOON
module VORTEX
module NE2K_PCI
if has PCMCIA; then
    module PCMCIA_AXNET
    module PCMCIA_PCNET
fi
module ADAPTEC_STARFIRE
module ET131X
module ACENIC
module ENA_ETHERNET
module AMD8111_ETH
case "$arch" in
    x86_64|i686|aarch64)
        enable NET_VENDOR_AMD
        module AMD_XGBE
    ;;
esac
if has PCMCIA; then
    module PCMCIA_NMCLAN
fi
module PCNET32
module AQTION
module ALX
module ATL1
module ATL1C
module ATL1E
module ATL2
module B44
module BNX2
module BNX2X
enable BNX2X_SRIOV
module BNXT
enable BNXT_FLOWER_OFFLOAD
enable BNXT_HWMON
enable BNXT_SRIOV
module CNIC
module NET_VENDOR_BROADCOM
module BNA
module CHELSIO_T1
enable CHELSIO_T1_1G
module CHELSIO_T3
module CHELSIO_T4
module CHELSIO_T4VF
module ENIC
enable NET_TULIP
module DE2104X
module DM9102
if has PCMCIA; then
    module PCMCIA_XIRCOM
fi
module TULIP
module ULI526X
module WINBOND_840
module DL2K
module SUNDANCE
module BE2NET
enable BE2NET_BE2
enable BE2NET_BE3
enable BE2NET_LANCER
enable BE2NET_SKYHAWK
module FEALNX
module GVE
module I40E
module I40EVF
module ICE
module IGBVF
module IGC
module IXGBE
enable IXGBE_HWMON
module IXGBEVF
module JME
module SKGE
enable SKGE_GENESIS
enable MLX4_CORE_GEN2
module MLX4_EN
module MLX5_CORE
enable MLX5_CORE_EN
enable MLX5_CORE_IPOIB
enable MLX5_EN_ARFS
enable MLX5_EN_RXNFC
enable MLX5_MPFS
module MLXFW
module KSZ884X_PCI
case "$arch" in
    x86_64)
        enable NET_VENDOR_MICROSOFT
        module MICROSOFT_MANA
    ;;
esac
enable NET_VENDOR_MYRI
module MYRI10GE
module NATSEMI
module NS83820
module S2IO
enable NET_VENDOR_NETRONOME
module NFP
module HAMACHI
module YELLOWFIN
module NETXEN_NIC
module QED
enable QED_SRIOV
module QEDE
enable NET_VENDOR_QLOGIC
module QLA3XXX
module QLCNIC
enable QLCNIC_HWMON
enable QLCNIC_SRIOV
module R6040
module 8139CP
module 8139TOO
enable 8139TOO_8129
enable NET_VENDOR_SOLARFLARE
module SFC_FALCON
module SFC
enable SFC_MCDI_MON
enable SFC_SRIOV
module SC92031
module SIS190
module SIS900
module EPIC100
if has PCMCIA; then
    module PCMCIA_SMC91C92
fi
module SMSC9420
case "$arch" in
    x86_64|i686)
        module DWMAC_INTEL
    ;;
esac
module STMMAC_ETH
module CASSINI
module HAPPYMEAL
module NIU
module SUNGEM
module TEHUTI
module TLAN
module VIA_RHINE
module VIA_VELOCITY
if has PCMCIA; then
    module PCMCIA_XIRC2PS
fi

# Common DRM drivers
module DRM_NOUVEAU
module DRM_RADEON
module DRM_AMDGPU
enable DRM_AMDGPU_SI
enable DRM_AMDGPU_CIK
enable DRM_AMD_DC
case "$arch" in
    x86_64|i686)
        enable DRM_AMD_DC_FP
        enable DRM_AMD_SECURE_DISPLAY
    ;;
esac
case "$arch" in
    x86_64|aarch64)
        enable HSA_AMD
    ;;
esac
enable DRM_AMD_DC_SI
enable DRM_RADEON_USERPTR
enable DRM_AMDGPU_USERPTR
enable DRM_AMD_ACP
module SND_SOC
module SND_SOC_AMD_ACP
case "$arch" in
    x86_64|i686)
        module DRM_I915
        module DRM_GMA500
    ;;
    aarch64)
        module DRM_PANFROST
        module DRM_PANEL_HIMAX_HX8394
    ;;
esac

# DRM for virtual machines
enable DRM_VIRTIO_GPU
case "$arch" in
    i686|x86_64)
        module DRM_VMWGFX
    ;;
esac
if has HYPERV; then
    module DRM_HYPERV
fi
if has VBOXGUEST; then
    module DRM_VBOXVIDEO
fi

# Common DMA drivers
case "$arch" in
    x86_64)
        module AMD_PTDMA
    ;;
esac

# FUSE
module CUSE
module FUSE_FS
module VIRTIO_FS

# iSCSI
enable SCSI_LOWLEVEL
module ISCSI_TCP
module SCSI_ISCSI_ATTRS

# Device mapper
module DM_CRYPT
module DM_INTEGRITY
enable DM_UEVENT
module DM_RAID
module DM_SNAPSHOT
module DM_VERITY
enable DM_VERITY_VERIFY_ROOTHASH_SIG
enable DM_VERITY_VERIFY_ROOTHASH_SIG_SECONDARY_KEYRING

# Firewire
module FIREWIRE
module FIREWIRE_NET
module FIREWIRE_NOSY
module FIREWIRE_OHCI
module FIREWIRE_SBP2

# GPIO
module GPIO_AMDPT

# USB
enable USB
enable USB_PHY
module USB_ROLE_SWITCH
module USB4
module USB4_NET
case "$arch" in
    x86_64|i686)
        module USB_LGM_PHY
        module USB_ROLES_INTEL_XHCI
    ;;
esac
module TYPEC
module TYPEC_UCSI
module TYPEC_MUX_PI3USB30532
module TYPEC_DP_ALTMODE
module TYPEC_NVIDIA_ALTMODE
module TYPEC_FUSB302
module TYPEC_TCPM
module TYPEC_TPS6598X

# USB audio
module SND_USB_AUDIO

# Common USB webcams
enable MEDIA_SUPPORT
enable MEDIA_USB_SUPPORT
enable MEDIA_CAMERA_SUPPORT
module USB_VIDEO_CLASS
module USB_GSPCA
module USB_GSPCA_BENQ
module USB_GSPCA_CONEX
module USB_GSPCA_CPIA1
module USB_GSPCA_DTCS033
module USB_GSPCA_ETOMS
module USB_GSPCA_FINEPIX
module USB_GSPCA_JEILINJ
module USB_GSPCA_JL2005BCD
module USB_GSPCA_KINECT
module USB_GSPCA_KONICA
module USB_GSPCA_MARS
module USB_GSPCA_MR97310A
module USB_GSPCA_NW80X
module USB_GSPCA_OV519
module USB_GSPCA_OV534
module USB_GSPCA_OV534_9
module USB_GSPCA_PAC207
module USB_GSPCA_PAC7302
module USB_GSPCA_PAC7311
module USB_GSPCA_SE401
module USB_GSPCA_SN9C2028
module USB_GSPCA_SN9C20X
module USB_GSPCA_SONIXB
module USB_GSPCA_SONIXJ
module USB_GSPCA_SPCA500
module USB_GSPCA_SPCA501
module USB_GSPCA_SPCA505
module USB_GSPCA_SPCA506
module USB_GSPCA_SPCA508
module USB_GSPCA_SPCA561
module USB_GSPCA_SPCA1528
module USB_GSPCA_SQ905
module USB_GSPCA_SQ905C
module USB_GSPCA_SQ930X
module USB_GSPCA_STK014
module USB_GSPCA_STK1135
module USB_GSPCA_STV0680
module USB_GSPCA_SUNPLUS
module USB_GSPCA_T613
module USB_GSPCA_TOPRO
module USB_GSPCA_TOUPTEK
module USB_GSPCA_TV8532
module USB_GSPCA_VC032X
module USB_GSPCA_VICAM
module USB_GSPCA_XIRLINK_CIT
module USB_GSPCA_ZC3XX
enable MEDIA_PCI_SUPPORT
if has ACPI && has I2C && has X86; then
    module IPU_BRIDGE
    module VIDEO_IPU3_CIO2
fi

# PHY controllers
enable GENERIC_PHY
enable NETWORK_PHY_TIMESTAMPING
module PHY_CAN_TRANSCEIVER
module ADIN_PHY
module AMD_PHY
module AQUANTIA_PHY
module AT803X_PHY
module BCM54140_PHY
module BCM7XXX_PHY
module BCM87XX_PHY
module BROADCOM_PHY
module CICADA_PHY
module CORTINA_PHY
module DAVICOM_PHY
module DP83822_PHY
module DP83848_PHY
module DP83869_PHY
module ICPLUS_PHY
module INTEL_XWAY_PHY
module LSI_ET1011C_PHY
module LXT_PHY
module MARVELL_10G_PHY
module MARVELL_88Q2XXX_PHY
module MARVELL_88X2222_PHY
module MARVELL_PHY
module MAXLINEAR_GPHY
module MEDIATEK_GE_PHY
module MICREL_PHY
module MICROCHIP_T1S_PHY
module MICROSEMI_PHY
module MOTORCOMM_PHY
module NATIONAL_PHY
module NCN26000_PHY
module NXP_C45_TJA11XX_PHY
module NXP_CBTX_PHY
module PHYLIB
module QSEMI_PHY
module SFP
module STE10XP
module TERANETICS_PHY

# NVME disks
enable NVME_HWMON
module BLK_DEV_NVME
module NVME_FC
module NVME_TCP

# Memory card readers
module MISC_RTSX_PCI
module MMC
module MMC_BLOCK
module MMC_REALTEK_PCI
module SDIO_UART
module MMC_CB710
module MISC_RTSX_USB
module MMC_REALTEK_USB
enable MMC_RICOH_MMC
module MMC_SDHCI
module MMC_SDHCI_ACPI
module MMC_SDHCI_PCI
if has PCMCIA; then
    module MMC_SDRICOH_CS
fi
module MMC_TIFM_SD
module MMC_TOSHIBA_PCI
module MMC_USHC
module MMC_VIA_SDMMC
module MMC_VUB300
if has ISA_DMA_API; then
    module MMC_WBSD
fi

# Needed by some bluetooth drivers
enable SERIAL_DEV_BUS
enable SERIAL_DEV_CTRL_TTYPORT

# Bluetooth
enable BT
enable BT_BREDR
enable BT_HS
enable BT_LE

module BT_ATH3K
module BT_BNEP
module BT_HCIBCM203X
module BT_HCIBFUSB
module BT_HCIBTUSB
module BT_HIDP
module BT_RFCOMM
if has PCMCIA; then
    module BT_HCIBLUECARD
    module BT_HCIBT3C
    module BT_HCIDTL1
fi
enable BT_HCIBTUSB_AUTOSUSPEND
enable BT_HCIBTUSB_MTK
enable BT_HCIUART_3WIRE
enable BT_HCIUART_AG6XX
enable BT_HCIUART_ATH3K
enable BT_HCIUART_BCM
enable BT_HCIUART_BCSP
enable BT_HCIUART_H4
enable BT_HCIUART_INTEL
enable BT_HCIUART_LL
enable BT_HCIUART_MRVL
enable BT_HCIUART_NOKIA
enable BT_HCIUART_QCA
enable BT_HCIUART_RTL
enable BT_LE_L2CAP_ECRED
enable BT_LEDS
enable BT_MSFTEXT
enable BT_RFCOMM_TTY
module BT_6LOWPAN
enable BT_AOSPEXT
enable BT_BNEP_MC_FILTER
enable BT_BNEP_PROTO_FILTER
module BT_HCIBCM4377
module BT_HCIBPA10X
module BT_HCIBTSDIO
module BT_HCIUART
module BT_HCIVHCI
module BT_MRVL
module BT_MRVL_SDIO
module BT_MTKSDIO
module BT_MTKUART
module BT_NXPUART
module BT_VIRTIO

# MIDI
module SND_SEQUENCER
module SND_TIMER
module SND_RAWMIDI

# Sound for HDA
module SND_HDA
remove SND_HDA_INTEL; module SND_HDA_INTEL # module only
enable SND_HDA_RECONFIG
enable SND_HDA_INPUT_BEEP
enable SND_HDA_PATCH_LOADER
module SND_HDA_CODEC_REALTEK
module SND_HDA_CODEC_ANALOG
module SND_HDA_CODEC_SIGMATEL
module SND_HDA_CODEC_VIA
module SND_HDA_CODEC_HDMI
module SND_HDA_CODEC_CIRRUS
module SND_HDA_CODEC_CONEXANT
module SND_HDA_CODEC_CA0110
module SND_HDA_CODEC_CA0132
enable SND_HDA_CODEC_CA0132_DSP
module SND_HDA_CODEC_CMEDIA
module SND_HDA_CODEC_SI3054

# Other sound
module SOUNDWIRE
enable SND_AC97_POWER_SAVE
module SND_AD1889
module SND_ALI5451
module SND_ALOOP
module SND_ALS300
module SND_ATIIXP_MODEM
module SND_ATIIXP
module SND_AU8810
module SND_AU8820
module SND_AU8830
module SND_AZT3328
module SND_BCD2000
module SND_BEBOB
module SND_BT87X
module SND_CA0106
module SND_CMIPCI
module SND_CS4281
enable SND_CS46XX_NEW_DSP
module SND_CS46XX
module SND_CTXFI
module SND_DARLA20
module SND_DARLA24
module SND_DICE
module SND_DUMMY
module SND_ECHO3G
module SND_EMU10K1X
module SND_EMU10K1
module SND_ENS1370
module SND_ENS1371
module SND_ES1938
module SND_ES1968
enable SND_ES1968_INPUT
enable MEDIA_RADIO_SUPPORT
enable SND_ES1968_RADIO
enable SND_FIREWIRE
module SND_FIREFACE
module SND_FIREWIRE_DIGI00X
module SND_FIREWIRE_MOTU
module SND_FIREWIRE_TASCAM
module SND_FIREWORKS
module SND_FM801
enable SND_FM801_TEA575X_BOOL
module SND_GINA20
module SND_GINA24
module SND_HDA_CODEC_CS8409
module SND_HDA_SCODEC_CS35L41_I2C
module SND_HDA_SCODEC_CS35L41_SPI
module SND_HDSPM
module SND_HDSP
module SND_ICE1712
module SND_ICE1724
module SND_INDIGODJX
module SND_INDIGODJ
module SND_INDIGOIOX
module SND_INDIGOIO
module SND_INDIGO
module SND_INTEL8X0M
module SND_INTEL8X0
module SND_ISIGHT
module SND_KORG1212
module SND_LAYLA20
module SND_LAYLA24
module SND_LOLA
module SND_LX6464ES
module SND_MAESTRO3
enable SND_MAESTRO3_INPUT
module SND_MIA
module SND_MIXART
module SND_MONA
module SND_MPU401
module SND_MTPAV
module SND_MTS64
module SND_NM256
module SND_OXFW
module SND_OXYGEN
module SND_PCXHR
module SND_PORTMAN2X4
module SND_RIPTIDE
module SND_RME32
module SND_RME9652
module SND_RME96
module SND_SERIAL_U16550
module SND_SOC_ES8316
module SND_SONICVIBES
module SND_TRIDENT
module SND_USB_6FIRE
module SND_USB_CAIAQ
enable SND_USB_CAIAQ_INPUT
module SND_USB_HIFACE
module SND_USB_PODHD
module SND_USB_POD
module SND_USB_TONEPORT
module SND_USB_UA101
module SND_USB_VARIAX
module SND_VIA82XX_MODEM
module SND_VIA82XX
module SND_VIRMIDI
module SND_VIRTUOSO
module SND_VX222
if has XEN; then
    module SND_XEN_FRONTEND
fi
if has ISA_DMA_API; then
    module SND_ALS4000
fi
case "$arch" in
    x86_64|i686)
        module SND_ASIHPI
        module SND_PCSP
        module SND_USB_US122L
    ;;
esac
case "$arch" in
    x86_64|i686|ppc64|ppc64le)
        module SND_USB_USX2Y
    ;;
esac

module SND_SOC
module SND_SOC_MAX98373_SDW
module SND_SOC_RT1308_SDW
module SND_SOC_RT5682_SDW
module SND_SOC_RT700_SDW
module SND_SOC_RT711_SDW
module SND_SOC_RT715_SDW
module SND_SOC_SOF_PCI
enable SND_SOC_SOF_TOPLEVEL

case "$arch" in
    x86_64|i686)
        enable SND_SOC_SOF_HDA_AUDIO_CODEC
        enable SND_SOC_SOF_HDA_LINK
        enable SND_SOC_SOF_INTEL_TOPLEVEL
        module SND_SOC_AMD_ACP3x
        module SND_SOC_AMD_ACP5x
        module SND_SOC_AMD_ACP6x
        module SND_SOC_AMD_CZ_DA7219MX98357_MACH
        module SND_SOC_AMD_CZ_RT5645_MACH
        module SND_SOC_AMD_RENOIR_MACH
        module SND_SOC_AMD_RENOIR
        module SND_SOC_AMD_VANGOGH_MACH
        module SND_SOC_AMD_YC_MACH
        module SND_SOC_INTEL_BDW_RT5650_MACH
        module SND_SOC_INTEL_BDW_RT5677_MACH
        module SND_SOC_INTEL_BROADWELL_MACH
        module SND_SOC_INTEL_BYT_CHT_CX2072X_MACH
        module SND_SOC_INTEL_BYT_CHT_DA7213_MACH
        module SND_SOC_INTEL_BYT_CHT_ES8316_MACH
        module SND_SOC_INTEL_BYTCR_RT5640_MACH
        module SND_SOC_INTEL_BYTCR_RT5651_MACH
        module SND_SOC_INTEL_CATPT
        module SND_SOC_INTEL_CHT_BSW_MAX98090_TI_MACH
        module SND_SOC_INTEL_CHT_BSW_NAU8824_MACH
        module SND_SOC_INTEL_CHT_BSW_RT5645_MACH
        module SND_SOC_INTEL_CHT_BSW_RT5672_MACH
        module SND_SOC_INTEL_GLK_RT5682_MAX98357A_MACH
        module SND_SOC_INTEL_HASWELL_MACH
        module SND_SOC_INTEL_KBL_DA7219_MAX98357A_MACH
        module SND_SOC_INTEL_KBL_RT5663_MAX98927_MACH
        module SND_SOC_INTEL_KBL_RT5663_RT5514_MAX98927_MACH
        module SND_SOC_INTEL_SKL_HDA_DSP_GENERIC_MACH
        module SND_SOC_INTEL_SKL_NAU88L25_MAX98357A_MACH
        module SND_SOC_INTEL_SKL_NAU88L25_SSM4567_MACH
        module SND_SOC_INTEL_SKL_RT286_MACH
        enable SND_SOC_INTEL_SKYLAKE_HDAUDIO_CODEC
        module SND_SOC_INTEL_SKYLAKE
        module SND_SOC_INTEL_SOF_RT5682_MACH
        module SND_SOC_INTEL_SOUNDWIRE_SOF_MACH
        enable SND_SOC_INTEL_USER_FRIENDLY_LONG_NAMES
    ;;
esac

# ACPI
enable ACPI_FFH
enable ACPI_PCI_SLOT
module ACPI_HED
module ACPI_IPMI
module ACPI_PFRUT
module ACPI_TAD

if has ARCH_ENABLE_MEMORY_HOTPLUG; then
    enable MEMORY_HOTPLUG
    enable ACPI_HOTPLUG_MEMORY
fi

if has ACPI_NUMA; then
    enable ACPI_HMAT
fi

case "$arch" in
    x86_64|aarch64)
        enable ACPI_FPDT
    ;;
esac

# acpi apei
if has HAVE_ACPI_APEI; then
    enable ACPI_APEI
    enable ACPI_APEI_GHES
    if has ARCH_SUPPORTS_MEMORY_FAILURE; then
        enable ACPI_APEI_MEMORY_FAILURE
    fi
    enable ACPI_APEI_PCIEAER
    module ACPI_APEI_EINJ

    case "$arch" in
        aarch64)
            enable ACPI_APEI_SEA
        ;;
    esac
fi

case "$arch" in
    x86_64|i686)
        module ACPI_EXTLOG
        module ACPI_PROCESSOR_AGGREGATOR
        module ACPI_SBS
    ;;
esac

# acpi pmic
case "$arch" in
    x86_64|i686)
        enable BXT_WC_PMIC_OPREGION
        enable BYTCRC_PMIC_OPREGION
        enable CHTCRC_PMIC_OPREGION
        enable CHT_DC_TI_PMIC_OPREGION
        enable CHT_WC_PMIC_OPREGION
        enable XPOWER_PMIC_OPREGION
    ;;
esac

# HID drivers
module HID_MULTITOUCH
module HID_GENERIC
if has ACPI && has I2C; then
    module I2C_HID_ACPI
fi
enable HIDRAW
module UHID

# CPU support
enable PINCTRL
if has IOMMU_SUPPORT; then
    enable IOMMUFD
fi
case "$arch" in
    x86_64|i686)
        # pinctrl_amd might not work as module if it is loaded to late
        enable PINCTRL_AMD
        enable INTEL_IOMMU
        enable IRQ_REMAP
        enable X86_AMD_PLATFORM_DEVICE
        enable CONFIG_X86_USER_SHADOW_STACK
    ;;
esac

if has ARCH_SUPPORTS_MEMORY_FAILURE; then
    enable MEMORY_FAILURE
fi

if has ARCH_SUPPORTS_SHADOW_CALL_STACK; then
    enable SHADOW_CALL_STACK
fi

if has HAVE_IRQ_TIME_ACCOUNTING; then
    enable IRQ_TIME_ACCOUNTING
fi

case "$arch" in
    x86_64|aarch64)
        enable CRYPTO_DEV_CCP
        enable CRYPTO_DEV_CCP_DD
    ;;
esac

case "$arch" in
    x86_64)
        enable ADDRESS_MASKING
        enable CRYPTO_DEV_SP_PSP
        module AMD_HSMP
        module NTB_AMD
    ;;
esac

# Performance monitoring
case "$arch" in
    x86_64|i686)
        enable PERF_EVENTS_AMD_BRS
        enable PERF_EVENTS_INTEL_CSTATE
        enable PERF_EVENTS_INTEL_RAPL
        enable PERF_EVENTS_INTEL_UNCORE
        module PERF_EVENTS_AMD_POWER
        module PERF_EVENTS_AMD_UNCORE
    ;;
esac

# Enable HW Random
enable HW_RANDOM
enable HW_RANDOM_VIRTIO
case "$arch" in
    x86_64|i686)
        enable HW_RANDOM_AMD
        enable HW_RANDOM_INTEL
    ;;
    aarch64|arm)
        enable HW_RANDOM_ARM_SMCCC_TRNG
    ;;
esac

# NVMEM support
# Needed for USB4 and Thunderbolt
enable MTD
enable NVMEM
module NVMEM_LAYOUT_ONIE_TLV
module NVMEM_LAYOUT_SL28_VPD
module NVMEM_RMEM

# I2C/SMBus
case "$arch" in
    x86_64|i686)
        module I2C_PIIX4
        enable I2C_DESIGNWARE_PLATFORM
        enable I2C_DESIGNWARE_BAYTRAIL
        enable I2C_DESIGNWARE_AMDPSP
        module MFD_INTEL_LPSS_PCI
        module MFD_INTEL_LPSS_ACPI
        module I2C_CHT_WC
        module I2C_ISMT
    ;;
esac
module I2C_AMD756
case "$arch" in
    x86_64|i686)
        module I2C_AMD756_S4882
    ;;
esac
module I2C_AMD8111
module I2C_AMD_MP2
module I2C_DESIGNWARE_PCI
module I2C_DIOLAN_U2C
module I2C_ISCH
module I2C_NFORCE2
case "$arch" in
    x86_64|i686)
        module I2C_NFORCE2_S4985
    ;;
esac
module I2C_PARPORT
module I2C_PCA_PLATFORM
module I2C_SCMI
module I2C_SIMTEC
module I2C_SIS96X
module I2C_TINY_USB
module I2C_VIA
module I2C_VIAPRO
module I2C_CHARDEV
module I2C_STUB

# RTL Network
module RTL8180
module RTL8187
module RTL8188EE
module RTL8192CE
module RTL8192CU
module RTL8192DE
module RTL8192EE
module RTL8192SE
module RTL8723AE
module RTL8723BE
module RTL8821AE

# USB Network interfaces
module USB_NET_DRIVERS
module USB_USBNET
module USB_NET_CDCETHER
module USB_NET_CDC_SUBSET
module USB_NET_RNDIS_HOST
module USB_RTL8150
module USB_RTL8152
module USB_NET_AQC111
module USB_NET_CDC_EEM
module USB_NET_CDC_MBIM
module USB_NET_CH9200
module USB_NET_CX82310_ETH
module USB_NET_DM9601
module USB_NET_GL620A
module USB_NET_HUAWEI_CDC_NCM
module USB_NET_INT51X1
module USB_NET_KALMIA
module USB_NET_MCS7830
module USB_NET_PLUSB
module USB_NET_QMI_WWAN
module USB_NET_RNDIS_WLAN
module USB_NET_SMSC75XX
module USB_NET_SMSC95XX
enable USB_ALI_M5632
enable USB_AN2720
module USB_CATC
enable USB_EPSON2888
module USB_HSO
module USB_IPHETH
module USB_KAWETH
enable USB_KC2190
module USB_LAN78XX
module USB_NET_SR9700
module USB_PEGASUS
module USB_SIERRA_NET
module USB_VL600

# Parallel ports
module PARPORT

# USB Serial
enable USB_SERIAL
module USB_SERIAL_SIMPLE
module USB_SERIAL_FTDI_SIO
module USB_SERIAL_CH341
module USB_ACM
module USB_SERIAL_AIRCABLE
module USB_SERIAL_ARK3116
module USB_SERIAL_BELKIN
module USB_SERIAL_CP210X
module USB_SERIAL_CYBERJACK
module USB_SERIAL_CYPRESS_M8
module USB_SERIAL_DEBUG
module USB_SERIAL_DIGI_ACCELEPORT
module USB_SERIAL_EDGEPORT_TI
module USB_SERIAL_EDGEPORT
module USB_SERIAL_EMPEG
module USB_SERIAL_F8153X
module USB_SERIAL_GARMIN
enable USB_SERIAL_GENERIC
module USB_SERIAL_IPAQ
module USB_SERIAL_IPW
module USB_SERIAL_IR
module USB_SERIAL_IUU
module USB_SERIAL_KEYSPAN_PDA
module USB_SERIAL_KEYSPAN
module USB_SERIAL_KLSI
module USB_SERIAL_KOBIL_SCT
module USB_SERIAL_MCT_U232
module USB_SERIAL_MOS7720
module USB_SERIAL_MOS7840
enable USB_SERIAL_MOS7715_PARPORT
module USB_SERIAL_NAVMAN
module USB_SERIAL_OMNINET
module USB_SERIAL_OPTICON
module USB_SERIAL_OPTION
module USB_SERIAL_OTI6858
module USB_SERIAL_PL2303
module USB_SERIAL_QCAUX
module USB_SERIAL_QT2
module USB_SERIAL_QUALCOMM
module USB_SERIAL_SAFE
module USB_SERIAL_SIERRAWIRELESS
module USB_SERIAL_SPCP8X5
module USB_SERIAL_SSU100
module USB_SERIAL_SYMBOL
module USB_SERIAL_TI
module USB_SERIAL_UPD78F0730
module USB_SERIAL_VISOR
module USB_SERIAL_WHITEHEAT
module USB_SERIAL_XR
module USB_SERIAL_XSENS_MT

# Required by podman
module TUN
enable CGROUP_HUGETLB
enable CGROUP_PIDS
enable CPUSETS
enable MEMCG
enable CGROUP_SCHED
enable BLK_CGROUP

# KVM
case "$arch" in
    x86_64|i686)
        enable PARAVIRT_SPINLOCKS
        enable PARAVIRT_TIME_ACCOUNTING
        module KVM
        module KVM_AMD
        module KVM_INTEL
    ;;
    aarch64)
        module KVM
    ;;
esac
module VHOST_VSOCK

# Useful filesystems
for fs in XFS BTRFS EXFAT VFAT F2FS ISO9660 UDF NTFS3; do
    module "${fs}_FS"
done
for fs in BTRFS F2FS NTFS3; do
    enable "${fs}_FS_POSIX_ACL"
done
enable "NTFS3_LZX_XPRESS"
enable "XFS_POSIX_ACL"
enable FS_ENCRYPTION
enable XFS_ONLINE_SCRUB
enable XFS_QUOTA
enable XFS_RT

# Needed for FAT filesystems
enable NLS_CODEPAGE_437
enable NLS_ISO8859_1

# Will be needed for composefs
enable FS_VERITY

# FS access notifications
enable FANOTIFY
enable FANOTIFY_ACCESS_PERMISSIONS

# Extcon
module EXTCON
case "$arch" in
    x86_64|i686)
        module EXTCON_AXP288
        module EXTCON_INTEL_CHT_WC
    ;;
esac

# Power management
enable PM
module CHARGER_BQ24190
module BATTERY_BQ27XXX
module BATTERY_MAX17042
case "$arch" in
    x86_64|i686)
        module AXP288_CHARGER
        module AXP288_FUEL_GAUGE
        module BATTERY_SURFACE
        module CHARGER_SURFACE
    ;;
esac

# Regulators
enable REGULATOR
module REGULATOR_FIXED_VOLTAGE
module REGULATOR_VIRTUAL_CONSUMER
module REGULATOR_USERSPACE_CONSUMER
module REGULATOR_88PG86X
module REGULATOR_ACT8865
module REGULATOR_AD5398
module REGULATOR_AW37503
module REGULATOR_AXP20X
module REGULATOR_DA9210
module REGULATOR_DA9211
module REGULATOR_FAN53555
module REGULATOR_GPIO
module REGULATOR_ISL9305
module REGULATOR_ISL6271A
module REGULATOR_LP3971
module REGULATOR_LP3972
module REGULATOR_LP872X
module REGULATOR_LP8755
module REGULATOR_LTC3589
module REGULATOR_LTC3676
module REGULATOR_MAX1586
module REGULATOR_MAX77857
module REGULATOR_MAX8649
module REGULATOR_MAX8660
module REGULATOR_MAX8893
module REGULATOR_MAX8952
module REGULATOR_MAX20086
module REGULATOR_MAX20411
module REGULATOR_MAX77826
module REGULATOR_MP8859
module REGULATOR_MT6311
module REGULATOR_PCA9450
module REGULATOR_PV88060
module REGULATOR_PV88080
module REGULATOR_PV88090
module REGULATOR_PWM
module REGULATOR_RAA215300
module REGULATOR_RT4801
module REGULATOR_RT4803
module REGULATOR_RT5190A
module REGULATOR_RT5739
module REGULATOR_RT5759
module REGULATOR_RT6160
module REGULATOR_RT6190
module REGULATOR_RT6245
module REGULATOR_RTQ2134
module REGULATOR_RTMV20
module REGULATOR_RTQ6752
module REGULATOR_RTQ2208
module REGULATOR_SLG51000
module REGULATOR_TPS51632
module REGULATOR_TPS62360
module REGULATOR_TPS65023
module REGULATOR_TPS6507X
module REGULATOR_TPS65132
module REGULATOR_TPS6524X
module RC_CORE

# PWM
enable PWM
case "$arch" in
    x86_64|i686)
        enable PWM_CRC
        module PWM_LPSS_PLATFORM
    ;;
esac

# RTC
case "$arch" in
    x86_64|i686)
        enable RTC_HCTOSYS
    ;;
esac

# MFD
case "$arch" in
    x86_64|i686)
        enable PMIC_OPREGION
        enable INTEL_SOC_PMIC
        module INTEL_SOC_PMIC_BXTWC
        module INTEL_SOC_PMIC_CHTDC_TI
        enable INTEL_SOC_PMIC_CHTWC
        module LPC_ICH
        module LPC_SCH
        module MFD_INTEL_PMC_BXT
    ;;
esac
module MFD_AXP20X_I2C

# RAM error correction
if has HAS_IOMEM && has EDAC_SUPPORT; then
    enable RAS
    module EDAC
fi

if has HAS_IOMEM; then
    module IPMI_HANDLER
fi

# PCI
if has HAVE_PCI; then
    enable PCI_IOV
    enable PCI_PRI
    enable PCI_PASID
    enable HOTPLUG_PCI
    enable HOTPLUG_PCI_PCIE
    enable PCIEPORTBUS
    #enable PCIE_PME
    enable PCIE_PTM
    enable NTB
    enable NTB_TRANSPORT
    if has RAS; then
        enable PCIEAER
        module PCIEAER_INJECT
        enable PCIEASPM
        enable PCIE_ECRC
        enable PCIE_DPC
        if has ACPI; then
            enable PCIE_EDR
        fi
    fi
fi

# RISC-V
case "$arch" in
    riscv*)
        # QEMU
        enable SOC_VIRT

        # Sifive Unmatched
        enable SOC_SIFIVE
        enable SIFIVE_CCACHE
        enable PCIE_FU740
        enable PCIE_MICROSEMI
        enable PCI_SW_SWITCHTEC
        module PWM_SIFIVE
        enable COMMON_CLK_PWM
        enable EDAC_SIFIVE
    ;;
esac

# Initramfs
enable DEVTMPFS
enable BLK_DEV_INITRD
for comp in GZIP BZIP2 LZMA XZ LZO LZ4 ZSTD; do
    enable RD_${comp}
done
remove DEBUG_BLOCK_EXT_DEVT

# Compressed firmware
enable FW_LOADER_COMPRESS

# IIO
module IIO
module ADXL372_I2C
module ADXL372_SPI
module BMC150_ACCEL
module DA280
module DA311
module DMARD10
module IIO_ST_ACCEL_3AXIS
module KXCJK1013
module MMA7660
module MMA8452
module MXC4005
module MXC6255
module AD7124
module AD7292
module AD7766
module AD7949
module AXP288_ADC
module MAX1241
module MAX1363
module MCP3911
module TI_ADC128S052
module TI_ADS1015
module AD5770R
module LTC1660
module TI_DAC7311
module ADXRS290
module BMG160
module FXAS21002C
module IIO_ST_GYRO_3AXIS
module MPU3050_I2C
module ADIS16475
module FXOS8700_I2C
module FXOS8700_SPI
module INV_ICM42600_I2C
module INV_ICM42600_SPI
module INV_MPU6050_I2C
module IIO_ST_LSM6DSX
module ACPI_ALS
module ADUX1020
module AL3010
module BH1750
module CM32181
module GP2AP002
module LTR501
module LV0104CS
module MAX44009
module OPT3001
module PA12203001
module RPR0521
module STK3310
module ST_UVIS25
module TSL2772
module VCNL4035
module VEML6030
module VL6180
module ZOPT2201
module AK8975
module BMC150_MAGN_I2C
module IIO_ST_MAGN_3AXIS
module SENSORS_RM3100_I2C
module SENSORS_RM3100_SPI
module ABP060MG
module BMP280
module ICP10100
module MPL115_I2C
module MB1232
module SX9310
module VCNL3020
module VL53L0X_I2C
module LTC2983
module MAX31856
module MAXIM_THERMOCOUPLE
module MLX90614
module MLX90632

# Accessibility
enable ACCESSIBILITY
enable A11Y_BRAILLE_CONSOLE
module SPEAKUP
module SPEAKUP_SYNTH_ACNTSA
module SPEAKUP_SYNTH_APOLLO
module SPEAKUP_SYNTH_AUDPTR
module SPEAKUP_SYNTH_BNS
module SPEAKUP_SYNTH_DECTLK
module SPEAKUP_SYNTH_LTLK
module SPEAKUP_SYNTH_SOFT
module SPEAKUP_SYNTH_SPKOUT
module SPEAKUP_SYNTH_TXPRT

# Joysticks
enable INPUT_JOYSTICK
# Joydev API
module INPUT_JOYDEV

# Joysticks (Xbox)
module JOYSTICK_XPAD
enable JOYSTICK_XPAD_FF
enable JOYSTICK_XPAD_LEDS

# Logitech game controllers
module HID_LOGITECH
module HID_LOGITECH_DJ
module HID_LOGITECH_HIDPP
enable LOGITECH_FF
enable LOGIRUMBLEPAD2_FF
enable LOGIG940_FF
enable LOGIWHEELS_FF

# Steam controller
module HID_STEAM

# Joydev API
module INPUT_JOYDEV

# Other joysticks
module JOYSTICK_A3D
module JOYSTICK_ADI
module JOYSTICK_ANALOG
module JOYSTICK_COBRA
module JOYSTICK_DB9
module JOYSTICK_GAMECON
module JOYSTICK_GF2K
module JOYSTICK_GRIP_MP
module JOYSTICK_GRIP
module JOYSTICK_GUILLEMOT
module JOYSTICK_IFORCE_232
module JOYSTICK_IFORCE_USB
module JOYSTICK_IFORCE
module JOYSTICK_INTERACT
module JOYSTICK_JOYDUMP
module JOYSTICK_MAGELLAN
module JOYSTICK_PXRC
module JOYSTICK_SIDEWINDER
module JOYSTICK_SPACEBALL
module JOYSTICK_SPACEORB
module JOYSTICK_STINGER
module JOYSTICK_TMDC
module JOYSTICK_TURBOGRAFX
module JOYSTICK_TWIDJOY
module JOYSTICK_WALKERA0701
module JOYSTICK_WARRIOR
module JOYSTICK_ZHENHUA

# gameport
module GAMEPORT_EMU10K1
module GAMEPORT_FM801

# Other human interfaces
module HID_ACCUTOUCH
module HID_ACRUX
enable HID_ACRUX_FF
module HID_ALPS
module HID_APPLEIR
module HID_ASUS
module HID_AUREAL
enable HID_BATTERY_STRENGTH
module HID_BETOP_FF
module HID_BIGBEN_FF
module HID_CMEDIA
module HID_CORSAIR
module HID_COUGAR
module HID_CP2112
module HID_CREATIVE_SB0540
module HID_DRAGONRISE
enable DRAGONRISE_FF
module HID_ELAN
module HID_ELECOM
module HID_ELO
module HID_EMS_FF
module HID_FT260
module HID_GEMBIRD
module HID_GFRM
module HID_GLORIOUS
module HID_GREENASIA
enable GREENASIA_FF
module HID_GT683R
module HID_HOLTEK
enable HOLTEK_FF
if has HYPERV; then
    module HID_HYPERV_MOUSE
fi
module HID_ICADE
module HID_JABRA
module HID_KEYTOUCH
module HID_KYE
module HID_LCPOWER
module HID_LED
module HID_LENOVO
module HID_MACALLY
module HID_MAGICMOUSE
module HID_MALTRON
module HID_MAYFLASH
module HID_MCP2221
module HID_NINTENDO
enable NINTENDO_FF
module HID_NTI
module HID_ORTEK
module HID_PENMOUNT
module HID_PICOLCD
module HID_PLANTRONICS
module HID_PLAYSTATION
enable PLAYSTATION_FF
module HID_PRIMAX
module HID_PRODIKEYS
module HID_RETRODE
module HID_RMI
module HID_ROCCAT
module HID_SAITEK
module HID_SEMITEK
module HID_SENSOR_ACCEL_3D
module HID_SENSOR_ALS
module HID_SENSOR_DEVICE_ROTATION
module HID_SENSOR_GYRO_3D
module HID_SENSOR_HUB
module HID_SENSOR_INCLINOMETER_3D
module HID_SENSOR_MAGNETOMETER_3D
module HID_SENSOR_TEMP
module HID_SMARTJOYPLUS
enable SMARTJOYPLUS_FF
module HID_SPEEDLINK
module HID_STEELSERIES
module HID_THINGM
module HID_THRUSTMASTER
enable THRUSTMASTER_FF
module HID_TIVO
module HID_TWINHAN
module HID_U2FZERO
module HID_UCLOGIC
module HID_UDRAW_PS3
module HID_VIEWSONIC
module HID_VIVALDI
module HID_WACOM
module HID_WALTOP
module HID_WIIMOTE
module HID_XINMO
module HID_ZEROPLUS
enable ZEROPLUS_FF
module HID_ZYDACRON
module HID_SONY
enable SONY_FF
case "$arch" in
    x86_64|i686)
        module INTEL_ISH_HID
        module SURFACE_HID
        module SURFACE_KBD
    ;;
esac
case "$arch" in
    x86_64)
        module AMD_SFH_HID
    ;;
esac

# Mice
module MOUSE_APPLETOUCH
module MOUSE_BCM5974
module MOUSE_CYAPA
module MOUSE_ELAN_I2C
enable MOUSE_ELAN_I2C_I2C
enable MOUSE_ELAN_I2C_SMBUS
enable MOUSE_PS2_ELANTECH
enable MOUSE_PS2_SENTELIC
case "$arch" in
    x86_64|i686)
        enable MOUSE_PS2_VMMOUSE
    ;;
esac
module MOUSE_SERIAL
module MOUSE_SYNAPTICS_I2C
module MOUSE_SYNAPTICS_USB
module MOUSE_VSXXXAA

# Make sure to enable virtual terminals in DRM
enable FB
enable FB_EFI
enable DRM_FBDEV_EMULATION
enable FRAMEBUFFER_CONSOLE

# Network
module BRIDGE
enable NETFILTER
enable IP_NF_IPTABLES
enable IP_NF_FILTER
enable IP_NF_TARGET_REJECT
module IP_NF_TARGET_MASQUERADE
enable IP_NF_MANGLE
enable NF_CONNTRACK
enable NF_NAT
module IP_NF_NAT
enable IP6_NF_IPTABLES
enable IP6_NF_MATCH_IPV6HEADER
enable IP6_NF_FILTER
enable IP6_NF_TARGET_REJECT
enable IP6_NF_MANGLE
module IP_NF_RAW
module IP6_NF_RAW
module IP6_NF_NAT
enable NETFILTER_ADVANCED
module VETH
module BLK_DEV_NBD
module 6LOWPAN
module IPVLAN

# Those two modules are required by multipass
module NETFILTER_XT_MATCH_COMMENT
module NETFILTER_XT_TARGET_CHECKSUM

# Security modules
enable SECURITY_SELINUX
enable SECURITY_APPARMOR
enable SECURITY_LANDLOCK
enable AUDIT

# SPI
if has HAS_IOMEM; then
    enable SPI
    module SPI_AMD
fi

# TPM
enable TCG_TPM
enable TCG_CRB
enable TCG_TIS
if has SPI; then
    enable TCG_TIS_SPI
fi

# BPF
enable BPF_SYSCALL
enable CGROUP_BPF
if has HAVE_CBPF_JIT || has HAVE_EBPF_JIT; then
    enable BPF_JIT
fi

# ZSWAP
enable ZSWAP

# Secure boot
enable SYSTEM_BLACKLIST_KEYRING
enable SYSTEM_TRUSTED_KEYRING
enable INTEGRITY_PLATFORM_KEYRING
enable INTEGRITY_SIGNATURE
enable INTEGRITY_ASYMMETRIC_KEYS
enable SECONDARY_TRUSTED_KEYRING
if has EFI; then
    enable INTEGRITY_MACHINE_KEYRING
    enable LOAD_UEFI_KEYS
fi
enable IMA
enable IMA_APPRAISE
enable IMA_SECURE_AND_OR_TRUSTED_BOOT
enable IMA_ARCH_POLICY

# Module signing
# /keys/linux-module-cert.crt is provided by downstream
if [ -f /keys/linux-module-cert.crt ]; then
    enable MODULE_SIG
    value_str SYSTEM_TRUSTED_KEYS /keys/linux-module-cert.crt
    # Building with MODULE_SIG_KEY is not reproducible
    value_str MODULE_SIG_KEY ""
    # We sign modules separately
    remove MODULE_SIG_ALL
    # Instead we use lsm=lockdown on command line
    remove MODULE_SIG_FORCE

    # sha512 signing only
    remove MODULE_SIG_SHA1
    remove MODULE_SIG_SHA224
    remove MODULE_SIG_SHA256
    remove MODULE_SIG_SHA384
    enable MODULE_SIG_SHA512
    value_str MODULE_SIG_HASH "sha512"

    enable SECURITY_LOCKDOWN_LSM
    enable SECURITY_LOCKDOWN_LSM_EARLY
else
    remove MODULE_SIG
fi

# systemd-oom
enable PSI

# Hardware monitors
case "$arch" in
    x86_64|i686)
        module SENSORS_K8TEMP
        module SENSORS_K10TEMP
        module SENSORS_FAM15H_POWER
        module SENSORS_NCT6683
        module SENSORS_NCT6775
        module SENSORS_NCT7802
        module SENSORS_NCT7904
        module SENSORS_NCT7904
    ;;
esac

# LEDS
enable LED_TRIGGER_PHY
enable LEDS_BRIGHTNESS_HW_CHANGED
enable LEDS_TRIGGERS
enable NEW_LEDS
module LEDS_CLASS
module LEDS_CLASS_FLASH
module LEDS_CLASS_MULTICOLOR
module LEDS_LP3944
module LEDS_LT3593
module LEDS_REGULATOR

enable LEDS_TRIGGER_DISK
enable LEDS_TRIGGER_PANIC
module LEDS_TRIGGER_ACTIVITY
module LEDS_TRIGGER_BACKLIGHT
module LEDS_TRIGGER_CAMERA
module LEDS_TRIGGER_DEFAULT_ON
module LEDS_TRIGGER_HEARTBEAT
module LEDS_TRIGGER_NETDEV
module LEDS_TRIGGER_ONESHOT
module LEDS_TRIGGER_PATTERN
module LEDS_TRIGGER_TIMER
module LEDS_TRIGGER_TRANSIENT

case "$arch" in
    x86_64|i686)
        module LEDS_APU
        module LEDS_INTEL_SS4200
    ;;
    aarch64)
        module LEDS_CLASS_FLASH
        module LEDS_SGM3140
    ;;
esac

# cpufreq
case "$arch" in
    x86_64|i686)
        enable CPU_FREQ_STAT
        enable X86_AMD_PSTATE
        module CPU_FREQ_GOV_CONSERVATIVE
        module CPU_FREQ_GOV_PERFORMANCE
        module CPU_FREQ_GOV_POWERSAVE
        module X86_AMD_FREQ_SENSITIVITY
        module X86_AMD_PSTATE_UT
        module X86_P4_CLOCKMOD
        module X86_PCC_CPUFREQ
        module X86_POWERNOW_K8
    ;;
    aarch64|arm)
        enable ACPI_CPPC_CPUFREQ_FIE
        enable ARM_PSCI_CPUIDLE_DOMAIN
        module ACPI_CPPC_CPUFREQ
        module ARM_SCPI_CPUFREQ
    ;;
esac

# Platforms
case "$arch" in
    aarch64|arm|x86_64|i686)
        # Chrome Platform
        enable CHROME_PLATFORMS
        module CHARGER_CROS_USBPD
        module CHROMEOS_ACPI
        module CHROMEOS_PRIVACY_SCREEN
        module CHROMEOS_TBMC
        module CROS_EC
        module CROS_EC_I2C
        module CROS_EC_SPI
        module CROS_EC_UART
        module CROS_EC_UART
        module CROS_HPS_I2C
        module CROS_HPS_I2C
        module CROS_KBD_LED_BACKLIGHT
        module CROS_USBPD_LOGGER

        # Mellanox Platform
        enable MELLANOX_PLATFORM
        module MLXREG_HOTPLUG
        module MLXREG_IO
        module MLXREG_LC
        module NVSW_SN2201
    ;;
esac

case "$arch" in
    x86_64|i686)
        enable MLX_PLATFORM
        module CHROMEOS_LAPTOP
        module CHROMEOS_PSTORE
        module CROS_EC_LPC
        module SURFACE_AGGREGATOR
        module SURFACE_AGGREGATOR_CDEV
        module SURFACE_AGGREGATOR_HUB
        module SURFACE_AGGREGATOR_TABLET_SWITCH
        module SURFACE_3_POWER_OPREGION
        module SURFACE_ACPI_NOTIFY
        module SURFACE_AGGREGATOR_REGISTRY
        module SURFACE_DTX
        module SURFACE_GPE
        module SURFACE_HOTPLUG
        module SURFACE_PLATFORM_PROFILE
        module SURFACE_PRO3_BUTTON
        module ACERHDF
        module ACER_WIRELESS
        module ACPI_TOSHIBA
        module AMD_PMC
        module AMD_PMF
        module AMILO_RFKILL
        module APPLE_GMUX
        module ASUS_LAPTOP
        module ASUS_WIRELESS
        module COMPAL_LAPTOP
        module FUJITSU_LAPTOP
        module FUJITSU_TABLET
        module GPD_POCKET_FAN
        module HP_ACCEL
        module IDEAPAD_LAPTOP
        module INTEL_ATOMISP2_PM
        module INTEL_CHTWC_INT33FE
        module INTEL_HID_EVENT
        module INTEL_INT0002_VGPIO
        module INTEL_OAKTRAIL
        module INTEL_RST
        module INTEL_SMARTCONNECT
        enable INTEL_TURBO_MAX_3
        module INTEL_VBTN
        module INTEL_IPS
        module INTEL_PMC_CORE
        module LG_LAPTOP
        module LENOVO_YMC
        module MSI_LAPTOP
        module MSI_EC
        module PANASONIC_LAPTOP
        module PCENGINES_APU2
        module SAMSUNG_LAPTOP
        module SAMSUNG_Q10
        module SENSORS_HDAPS
        module SERIAL_MULTI_INSTANTIATE
        module SYSTEM76_ACPI
        module SONY_LAPTOP
        enable SONYPI_COMPAT
        module THINKPAD_ACPI
        enable THINKPAD_ACPI_ALSA_SUPPORT
        enable THINKPAD_ACPI_HOTKEY_POLL
        enable THINKPAD_ACPI_VIDEO
        enable THINKPAD_LMI
        module TOPSTAR_LAPTOP
        module TOSHIBA_BT_RFKILL
        module TOSHIBA_HAPS
        module TOSHIBA_WMI
        module WIRELESS_HOTKEY
        module YOGABOOK
        module ACPI_WMI
        module ACER_WMI
        module GIGABYTE_WMI
        enable X86_PLATFORM_DRIVERS_DELL
        enable X86_PLATFORM_DRIVERS_HP
        module ALIENWARE_WMI
        module ASUS_NB_WMI
        module ASUS_WMI
        module ASUS_TF103C_DOCK
        enable DELL_SMBIOS_WMI
        module DELL_WMI_AIO
        module DELL_WMI_LED
        module DELL_WMI
        module EEEPC_WMI
        module HP_WMI
        module HUAWEI_WMI
        module INTEL_WMI_THUNDERBOLT
        module MSI_WMI
        module NVIDIA_WMI_EC_BACKLIGHT
        module SENSORS_ASUS_WMI
        module SURFACE3_WMI
        module X86_ANDROID_TABLETS
    ;;
esac

# power
enable ENERGY_MODEL
enable POWER_RESET
enable POWER_RESET_RESTART
enable POWER_SUPPLY
enable POWER_SUPPLY_HWMON

# power/supply
module BATTERY_CW2015
module BATTERY_RT5033
module BATTERY_UG3105
module CHARGER_BD99954
module CHARGER_BQ2515X
module CHARGER_BQ256XX
module CHARGER_BQ25890
module CHARGER_LT3651
module CHARGER_LTC4162L
module CHARGER_MAX77976
module CHARGER_RT9467
module CHARGER_RT9471
module CHARGER_SMB347

# Powercap
enable POWERCAP
enable IDLE_INJECT
case "$arch" in
    x86_64|i686)
        module INTEL_RAPL
    ;;
esac
